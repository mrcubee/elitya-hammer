package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.reflect.Method;

public class ObjectMethod {
	private Object object;
	private Method method;
	
	public ObjectMethod(Object object, Method method) {
		this.object = object;
		this.method = method;
	}
	
	public Object getObject() {
		return object;
	}
	
	public Method getMethod() {
		return method;
	}
}
