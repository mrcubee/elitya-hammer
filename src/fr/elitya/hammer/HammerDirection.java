package fr.elitya.hammer;

public enum HammerDirection {
	
	UP(1, 0, 1),
	DOWN(1, 0, 1),
	NORTH(1, 1, 0),
	SOUTH(1, 1, 0),
	EAST(0, 1, 1),
	WEST(0, 1, 1);
	
	private int x;
	private int y;
	private int z;
	
	private HammerDirection(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}

}
