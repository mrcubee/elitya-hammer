package fr.elitya.hammer;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import fr.elitya.hammer.listener.BlockBreak;
import fr.mrcubee.plugin.spigot.utils.PluginAnnotation;
import fr.mrcubee.plugin.spigot.utils.itemstack.ItemStackBuilder;

/**
 * 
 * @author MrCubee
 *
 */
public class Hammer extends JavaPlugin {
	
	private static Hammer instance;

	private List<Player> playersUseHammer;
	private ItemStack hammerItem;
	
	@Override
	public void onLoad() {
		playersUseHammer = new ArrayList<Player>();
	}

	@Override
	public void onEnable() {
		BlockBreak blockBreak = new BlockBreak();
		
		saveDefaultConfig();
		PluginAnnotation.loadAnnotations(this, blockBreak);
		this.instance = this;
		if (this.hammerItem == null)
			this.hammerItem = ItemStackBuilder.create("Hammer", Material.DIAMOND_PICKAXE, ChatColor.RED + "[Hammer]");
		this.getServer().getPluginManager().registerEvents(blockBreak, this);
	}

	@Override
	public void onDisable() {
		
	}
	
	public boolean isPlayerUseHammer(Player player) {
		return (player == null) ? false : playersUseHammer.contains(player);
	}
	
	public void addPlayer(Player player) {
		if (player != null && !isPlayerUseHammer(player))
			playersUseHammer.add(player);
	}
	
	public void removePlayer(Player player) {
		playersUseHammer.remove(player);
	}
	
	public ItemStack getHammerItem() {
		return hammerItem.clone();
	}
	
	public static Hammer getInstance() {
		return instance;
	}

}
