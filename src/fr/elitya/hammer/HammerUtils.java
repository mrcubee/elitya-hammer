package fr.elitya.hammer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import fr.mrcubee.plugin.spigot.utils.ServerVersion;

public class HammerUtils {

	private static Object createBlockPosition(int x, int y, int z) {
		Class<?> clazz = null;
		Object object = null;

		try {
			clazz = Class.forName("net.minecraft.server." + ServerVersion.getPackageVersion() + ".BlockPosition");
			if (clazz != null)
				return false;
			object = clazz.getConstructor(Integer.class, Integer.class, Integer.class).newInstance(x, y, z);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {}
		return object;
	}

	private static Object getCraftPlayer(Player player) {
		Class<?> clazz = null;
		Object object = null;

		try {
			clazz = Class.forName("org.bukkit.craftbukkit." + ServerVersion.getPackageVersion() + ".entity.CraftPlayer");
		} catch (ClassNotFoundException e) {}
		if (clazz == null || !clazz.isInstance(player))
			return null;
		object = clazz.cast(player);
		return object;
	}

	private static Object getEntityPlayer(Player player) {
		Object craftPlayer = getCraftPlayer(player);
		Object entityPlayer = null;
		
		if (craftPlayer == null)
			return null;
		try {
			entityPlayer = craftPlayer.getClass().getMethod("getHandle").invoke(craftPlayer);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {}
		return entityPlayer;
	}
	
	private static Object getPlayerInteractManager(Player player) {
		Object entityPlayer = getEntityPlayer(player);
		Object playerInteractManager = null;
		
		if (entityPlayer == null)
			return null;
		try {
			playerInteractManager = entityPlayer.getClass().getField("playerConnection").get(entityPlayer);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {}
		return playerInteractManager;
	}
	
	private static void breakBlocks(Player player, List<Block> blocks) {
		Object playerInteractManager = getPlayerInteractManager(player);
		Class blockPositionClass = null;
		Method breakBlockMethod = null;
		Object blockPosition;
		
		if (playerInteractManager == null)
			return;
		try {
			blockPositionClass = Class.forName("net.minecraft.server." + ServerVersion.getPackageVersion() + ".BlockPosition");
		} catch (ClassNotFoundException e1) {}
		if (blockPositionClass == null)
			return;
		try {
			breakBlockMethod = playerInteractManager.getClass().getMethod("breakBlock", blockPositionClass);
		} catch (NoSuchMethodException | SecurityException e) {}
		if (breakBlockMethod == null)
			return;
		for (Block block : blocks) {
			blockPosition = createBlockPosition(block.getX(), block.getY(), block.getZ());
			if (blockPosition != null) {
				try {
					breakBlockMethod.invoke(playerInteractManager, blockPosition);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
			}
		}
	}

	public static void hammerBreak(Player player, Block block, HammerDirection hammerDirection, int size) {
		int midle_size = size / 2;
		int sx = block.getX() - (midle_size * hammerDirection.getX());
		int sy = block.getY() - (midle_size * hammerDirection.getY());
		int sz = block.getZ() - (midle_size * hammerDirection.getZ());
		List<Block> blocks = new ArrayList<Block>();
		
		for (int y = 0; y < size; y++)
			for (int z = 0; z < size; z++)
				for (int x = 0; x < size; x++)
					blocks.add(block.getWorld().getBlockAt(sx + (x * hammerDirection.getX()), sy + (y * hammerDirection.getY()),
							sz + (z * hammerDirection.getZ())));
		Hammer.getInstance().addPlayer(player);
		breakBlocks(player, blocks);
		Hammer.getInstance().removePlayer(player);
	}

}
