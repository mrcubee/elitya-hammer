package fr.elitya.hammer.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.elitya.hammer.Hammer;
import fr.elitya.hammer.HammerDirection;
import fr.elitya.hammer.HammerUtils;
import fr.mrcubee.plugin.spigot.utils.annotation.Config;


/***
 * 
 * @author MrCubee
 *
 */
public class BlockBreak implements Listener {
	
	@Config(path = "hammer.size")
	private int hammer_size = 3;
	
	@EventHandler
	public void event(BlockBreakEvent event) {
		Hammer hammerPlugin = Hammer.getInstance();
		
		if (event.getPlayer().getItemInHand() == null
				|| event.getPlayer().getItemInHand().isSimilar(hammerPlugin.getHammerItem())
				|| hammerPlugin.isPlayerUseHammer(event.getPlayer()))
			return;
		HammerUtils.hammerBreak(event.getPlayer(), event.getBlock(), HammerDirection.valueOf(null), hammer_size);
	}
	
}
